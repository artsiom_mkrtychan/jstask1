 'use strict';

    //product base class
    function Product() {
      this.make = "";
      this.model = "";
      this.count = 0;
      this.price = 0;
    }

    //info
    Product.prototype.getInfo = function() {

      return "  make:" + this.make + "\n" +
          "  model:" + this.model  + "\n" +
          "  count:" + this.count + "\n" +
          "  price:" + this.price + "\n";
    }

    //totalPrice
    Product.prototype.getTotalPrice = function() {
      return this.price * this.count;
    }

    //CellPhone inherite class
    function CellPhone() {
      Product.apply(this, arguments);
      this.camera = false;
      this.memory = 0;
    }
    CellPhone.prototype = Object.create(Product.prototype);
    CellPhone.prototype.constructor = CellPhone;

    CellPhone.prototype.getInfo = function() {
      var defaultInfo = Product.prototype.getInfo.apply(this, arguments);
      return defaultInfo +
                "  camera:" + this.camera + "\n" +
                "  memory:" + this.memory + "\n";
    }

    CellPhone.prototype.getTotalPrice = function() {
      var defaultPrice = Product.prototype.getTotalPrice.apply(this, arguments);
      var tax = defaultPrice * this.memory / 100;

      return defaultPrice + tax;
    }

    //Printer inherite class
    function Printer() {
      Product.apply(this, arguments);
      this.printSpeed = 0;
      this.paperTrayCapacity = 0;
    }
    Printer.prototype = Object.create(Product.prototype);
    Printer.prototype.constructor = CellPhone;

    Printer.prototype.getInfo = function() {
      var defaultInfo = Product.prototype.getInfo.apply(this, arguments);
      return defaultInfo +
                "  printSpeed:" + this.printSpeed + "\n" +
                "  paperTrayCapacity:" + this.paperTrayCapacity + "\n";
    }

    //WashingMachine inherite class
    function WashingMachine() {
      Product.apply(this, arguments);
      this.maxLoad = 0;
      this.spinSpeed = 0;
    }
    WashingMachine.prototype = Object.create(Product.prototype);
    WashingMachine.prototype.constructor = CellPhone;

    WashingMachine.prototype.getInfo = function() {
      var defaultInfo = Product.prototype.getInfo.apply(this, arguments);
      return defaultInfo +
                "  maxLoad:" + this.maxLoad + "\n" +
                "  spinSpeed:" + this.spinSpeed + "\n";
    }


    function protoStyle(){
        console.clear()

        var product1 = new Product();
        var product2 = new Product();

        product1.make = "makeProduct1";
        product1.model = "modelProduct1";
        product1.count = 2;
        product1.price = 10;

        product2.make = "makeProduct2";
        product2.model = "modelProduct2";
        product2.count = 4;
        product2.price = 20;


        var cellPhone1 = new CellPhone();
        var cellPhone2 = new CellPhone();

        cellPhone1.make = "makeCellPhone1";
        cellPhone1.model = "modelCellPhone1";
        cellPhone1.count = 2;
        cellPhone1.price = 10;
        cellPhone1.camera = true;
        cellPhone1.memory = 10;

        cellPhone2.make = "makeCellPhone2";
        cellPhone2.model = "modelCellPhone2";
        cellPhone2.count = 4;
        cellPhone2.price = 20;
        cellPhone2.camera = false;
        cellPhone2.memory = 20;


        var printer1 = new Printer();
        var printer2 = new Printer();

        printer1.make = "makePrinter1";
        printer1.model = "modelPrinter1";
        printer1.count = 2;
        printer1.price = 10;
        printer1.printSpeed = 10;
        printer1.paperTrayCapacity = 10;

        printer2.make = "makePrinter2";
        printer2.model = "modelPrinter2";
        printer2.count = 4;
        printer2.price = 20;
        printer2.printSpeed = 5;
        printer2.paperTrayCapacity = 20;


        var washingMachine1 = new WashingMachine();
        var washingMachine2 = new WashingMachine();

        washingMachine1.make = "makeWashingMachine1";
        washingMachine1.model = "modelWashingMachine1";
        washingMachine1.count = 2;
        washingMachine1.price = 10;
        washingMachine1.maxLoad = 5;
        washingMachine1.spinSpeed = 100;

        washingMachine2.make = "makeWashingMachine2";
        washingMachine2.model = "modelWashingMachine2";
        washingMachine2.count = 4;
        washingMachine2.price = 20;
        washingMachine2.maxLoad = 15;
        washingMachine2.spinSpeed = 2000;

        console.log ("Proto style:\n");


        var products = [];
        products.push(product1);
        products.push(product1);
        products.push(cellPhone1);
        products.push(cellPhone2);
        products.push(printer1);
        products.push(printer2);
        products.push(washingMachine1);
        products.push(washingMachine2);

        for (var i = 0; i < products.length; i++) {
            console.log ("\nInfo:");
            console.log (products[i].getInfo());
            console.log ("totalPrice:" + products[i].getTotalPrice());
        }
    }



    class ClassProduct {
        constructor(){
            this.make = "";
            this.model = "";
            this.count = 0;
            this.price = 0;
        }
        getInfo() {
            return "  make:" + this.make + "\n" +
            "  model:" + this.model  + "\n" +
            "  count:" + this.count + "\n" +
            "  price:" + this.price + "\n";
        }
        getTotalPrice() {
            return this.price * this.count;
        }
    }


    class ClassCellPhone extends ClassProduct{
        constructor(){
            super();
            this.camera = false;
            this.memory = 0;
        }
        getInfo() {
            var defaultInfo = super.getInfo();
            return defaultInfo +
                "  camera:" + this.camera + "\n" +
                "  memory:" + this.memory + "\n";
        }

        getTotalPrice() {
            var defaultPrice = super.getTotalPrice();
            var tax = defaultPrice * this.memory / 100;

            return defaultPrice + tax;
        }
    }

    class ClassPrinter extends ClassProduct{
        constructor(){
            super();
            this.printSpeed = 0;
            this.paperTrayCapacity = 0;
        }
        getInfo() {
            var defaultInfo = super.getInfo();
            return defaultInfo +
               "  printSpeed:" + this.printSpeed + "\n" +
               "  paperTrayCapacity:" + this.paperTrayCapacity + "\n";
        }        
    }

    class ClassWashingMachine extends ClassProduct{
        constructor(){
            super();
             this.maxLoad = 0;
            this.spinSpeed = 0;
        }
        getInfo() {
            var defaultInfo = super.getInfo();
            return defaultInfo +
                "  maxLoad:" + this.maxLoad + "\n" +
                "  spinSpeed:" + this.spinSpeed + "\n";
        }       
    }


    function classStyle(){
        console.clear();

        var product1 = new ClassProduct();
        var product2 = new ClassProduct();

        product1.make = "makeProduct1";
        product1.model = "modelProduct1";
        product1.count = 2;
        product1.price = 10;

        product2.make = "makeProduct2";
        product2.model = "modelProduct2";
        product2.count = 4;
        product2.price = 20;


        var cellPhone1 = new ClassCellPhone();
        var cellPhone2 = new ClassCellPhone();

        cellPhone1.make = "makeCellPhone1";
        cellPhone1.model = "modelCellPhone1";
        cellPhone1.count = 2;
        cellPhone1.price = 10;
        cellPhone1.camera = true;
        cellPhone1.memory = 10;

        cellPhone2.make = "makeCellPhone2";
        cellPhone2.model = "modelCellPhone2";
        cellPhone2.count = 4;
        cellPhone2.price = 20;
        cellPhone2.camera = false;
        cellPhone2.memory = 20;
        
        
        var printer1 = new ClassPrinter();
        var printer2 = new ClassPrinter();

        printer1.make = "makePrinter1";
        printer1.model = "modelPrinter1";
        printer1.count = 2;
        printer1.price = 10;
        printer1.printSpeed = 10;
        printer1.paperTrayCapacity = 10;

        printer2.make = "makePrinter2";
        printer2.model = "modelPrinter2";
        printer2.count = 4;
        printer2.price = 20;
        printer2.printSpeed = 5;
        printer2.paperTrayCapacity = 20;


        var washingMachine1 = new ClassWashingMachine();
        var washingMachine2 = new ClassWashingMachine();

        washingMachine1.make = "makeWashingMachine1";
        washingMachine1.model = "modelWashingMachine1";
        washingMachine1.count = 2;
        washingMachine1.price = 10;
        washingMachine1.maxLoad = 5;
        washingMachine1.spinSpeed = 100;

        washingMachine2.make = "makeWashingMachine2";
        washingMachine2.model = "modelWashingMachine2";
        washingMachine2.count = 4;
        washingMachine2.price = 20;
        washingMachine2.maxLoad = 15;
        washingMachine2.spinSpeed = 2000;
        
        console.log ("Class style:\n");
        
        var products = [];
        products.push(product1);
        products.push(product1);
        products.push(cellPhone1);
        products.push(cellPhone2);
        products.push(printer1);
        products.push(printer2);
        products.push(washingMachine1);
        products.push(washingMachine2);

        for (var i = 0; i < products.length; i++) {
            console.log ("\nInfo:");
            console.log (products[i].getInfo());
            console.log ("totalPrice:" + products[i].getTotalPrice());
        }
    }






